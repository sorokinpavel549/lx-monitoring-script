import psutil
import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import logging
#
logging.basicConfig(filemode='a', filename='scipt.log', format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
from logging.handlers import RotatingFileHandler
handler = RotatingFileHandler(filename='scipt.log', maxBytes=1024*1024*5)

import argparse
parser = argparse.ArgumentParser(description="Monitoring_script", add_help=False)
parser.add_argument('-n', '--nochanges', action='store_true', help="shows output")
parser.add_argument('-y', '--changes', action='store_true', help="shows output")
args = parser.parse_args()


if args.changes == False and args.nochanges == False:
    import module_for_script
    print('Threshold parameters for CPU is', module_for_script.cpu_percent, '%, for RAM is',
          module_for_script.ram_percent, '%, for DISK is', module_for_script.disk_percent, '%.')

if args.nochanges:
    import module_for_script
    print('Threshold parameters for CPU is',module_for_script.cpu_percent,'%, for RAM is',module_for_script.ram_percent,'%, for DISK is',module_for_script.disk_percent,'%.')

if args.changes:
    print('Please enter threshold parameters for load CPU in %:')
    a = int(input())
    print('Please enter threshold parameters for load RAM in %:')
    b = int(input())
    print('Please enter threshold parameters for DISK SPACE usage in %:')
    c = int(input())
    print('Please enter threshold parameters for DISK I/O usage in %:')
    d = int(input())

    with open('module_for_script.py', 'w') as file:
        file.write('cpu_percent=' + str(a) + '\n' + 'ram_percent=' + str(b) + '\n' + 'disk_percent=' + str(c) + '\n' + 'diskio_percent=' + str(d))
        file.close()
    import module_for_script

def main():
    count = 0
#    print('Load Average stats:', get_loadaverage())
    print('CPU usage is', get_cpu_usage_pct(),'%.')
    print('RAM usage is', (get_ram_usage_pct()), '%, it is', (get_ram_usage()), 'MB of total',
          (get_ram_total()), 'MB.')
    print('Disk usage is', (get_disk_usage_pct()), '%, free disk space is', (get_disk_free()), 'MB, of',
          (get_disk_total()), 'MB.')
    print('Disk I/O usage is', get_diskio(),'%')

    if get_cpu_usage_pct() > module_for_script.cpu_percent:
        print('WARNING: CPU usage more than',module_for_script.cpu_percent,'%')
        cpu = module_for_script.cpu_percent
        logging.warning('WARNING: CPU usage more than %s percents', cpu)
        count+=1

    if get_ram_usage_pct() > module_for_script.ram_percent:
        print('WARNING: MEMORY usage more than',module_for_script.ram_percent,'%')
        ram = module_for_script.ram_percent
        logging.warning('WARNING: MEMORY usage more than %s percents', ram)
        count+=1

    if get_disk_usage_pct() > module_for_script.disk_percent:
        print('WARNING: DISK SPACE usage more than',module_for_script.disk_percent,'%')
        disk = module_for_script.disk_percent
        logging.warning('WARNING: DISK SPACE usage more than %s percents', disk)
        count+=1
    if get_diskio() > module_for_script.diskio_percent:
        print('WARNING: DISK I/O usage more than',module_for_script.diskio_percent,'%')
        diskio = module_for_script.diskio_percent
        logging.warning('WARNING: DISK I/O usage more than %s percents', diskio)
        count+=1

    if count > 0:
        print('Try to send message.')
        mailer()



def get_ram_usage():
    return psutil.virtual_memory().total - psutil.virtual_memory().available >> 20


def get_ram_total():
    return psutil.virtual_memory().total >> 20


def get_ram_usage_pct():
    return psutil.virtual_memory().percent


def get_cpu_usage_pct():
    return psutil.cpu_percent(interval=0.5)


def get_disk_usage_pct():
    return psutil.disk_usage('/').percent


def get_disk_free():
    return psutil.disk_usage('/').free >> 20


def get_disk_total():
    return psutil.disk_usage('/').total >> 20


def get_loadaverage():
    return psutil.getloadavg()

def get_diskio():
    p = psutil.Process()
    io_counters = p.io_counters()
    disk_usage_process = io_counters[2] + io_counters[3]  # read_bytes + write_bytes
    disk_io_counter = psutil.disk_io_counters()
    disk_total = disk_io_counter[2] + disk_io_counter[3]  # read_bytes + write_bytes
    total_io_percent = ((disk_usage_process / disk_total) * 100)
    return total_io_percent



def mailer():
    sender_email = "leverx.monitoring.script@gmail.com"
    receiver_email = "pavel.sarokin@leverx.com"
    password = ""

    message = MIMEMultipart("alternative")
    message["Subject"] = "Warning"
    message["From"] = sender_email
    message["To"] = receiver_email

    html = """<html>
Hello, DISK space usage {link}%, CPU usage {link2}%, RAM usage {link3}%, Disk I/O usage {link4}%!
</html>
""".format(link=get_disk_usage_pct(), link2=get_cpu_usage_pct(), link3=get_ram_usage_pct(), link4=get_diskio())



    part2 = MIMEText(html, "html")

    message.attach(part2)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(
            sender_email, receiver_email, message.as_string()
        )
    print('Warning message send successful.')

if __name__ == "__main__":
    main()



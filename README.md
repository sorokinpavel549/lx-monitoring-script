# LX-monitoring-script

This script allows you to check the values of CPU, RAM, IO and Free space of the DISK.

You can run the script with the **-y** parameter in order to set the threshold values you need, after which the script will send notifications to your email:

**monitoring_script.py -y**

These threshold parameters are stored in a file module_for_script.py

Important messages are stored in a file script.log
##
Dependencies:
**sudo apt-get install -y python3-psutil**
##
For running python script at every 5th minute:

**crontab -e**

*/5 * * * * /usr/bin/python3 $YOUR_FOLDER_TO_SCRIPT/monitoring_script.py -n
